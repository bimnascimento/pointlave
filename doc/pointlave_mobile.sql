/*
Navicat MySQL Data Transfer

Source Server         : CPRO41598-POINTLAVE
Source Server Version : 50718
Source Host           : cpro41598.publiccloud.com.br:3306
Source Database       : pointlave_mobile

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2018-03-06 22:56:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pointlave_mobile
-- ----------------------------
DROP TABLE IF EXISTS `pointlave_mobile`;
CREATE TABLE `pointlave_mobile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cidade` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `img` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `qnt` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pointlave_mobile
-- ----------------------------
INSERT INTO `pointlave_mobile` VALUES ('1', 'Juiz de Fora', 'Minas Gerais', 'https://www.pointlave.com.br/wp-content/uploads/juiz-de-fora.png', 'https://www.pointlave.com.br/juiz-de-fora/', '6', '1');
INSERT INTO `pointlave_mobile` VALUES ('2', 'Três Rios', 'Rio de Janeiro', 'https://www.pointlave.com.br/wp-content/uploads/tres-rios.png', 'https://www.pointlave.com.br/tres-rios/', '2', '0');
